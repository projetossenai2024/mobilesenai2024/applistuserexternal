import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import React from 'react'

const Home = () => {
    return (
        <View style={styles.container}>
            <View style={styles.sectionContainer}>
                <View style={styles.sectionInContainer}>
                    <Text style={styles.sectionTitle}>Section 1</Text>
                </View>

                <View style={styles.sectionInContainer}>
                    <Text style={styles.sectionTitle}>Section 2</Text>
                </View>
            </View>

            <View style={styles.section}>
                <Text style={styles.sectionTitle}>Section 2</Text>
            </View>

            <View style={styles.sectionCircle}>
                <ScrollView horizontal={true}>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                    <View style={styles.sectionInCircle}>
                        <Text style={styles.sectionTitle}>S</Text>
                    </View>
                </ScrollView>
            </View>


        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        alignItems: 'center'
    },
    sectionContainer: {
        flexDirection: 'row'
    },
    sectionInContainer: {
        flex: 1,
        width: 100,
        padding: 40,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        marginTop: 15,
        backgroundColor: '#89ade9',
        borderRadius: 8
    },
    sectionCircle: {
        flexDirection: 'row',
        marginTop: 15,
    },
    sectionInCircle: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        backgroundColor: '#89ade9',
        width: 50,
        height: 50,
        borderRadius: 25
    },
    section: {
        width: '100%',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        marginTop: 15,
        backgroundColor: '#89ade9',
        borderRadius: 8
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5
    },

})