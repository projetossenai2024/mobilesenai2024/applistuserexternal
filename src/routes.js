import React from 'react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

//Pages
import Home from './pages/home';
import List from './pages/List';
import Details from './pages/Details';

//Icons
import { Ionicons } from '@expo/vector-icons'







const Tab = createBottomTabNavigator();

function Routes() {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: 'white',
                tabBarInactiveTintColor: '#f1f1f1',
                tabBarActiveBackgroundColor:'black',
                tabBarInactiveBackgroundColor:'black',
                
                tabBarShowLabel: false,
                tabBarStyle:{
                    position: 'absolute',
                    backgroundColor: '#171626',
                    borderTopWidth: 0.15,
                }
            }}
        >
            <Tab.Screen
                name="home"
                component={Home}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="home" size={size} color={color}/>
                        }

                        return <Ionicons name="home-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="List"
                component={List}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="reader" size={size} color={color}/>
                        }

                        return <Ionicons name="reader-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="Details"
                component={Details}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="albums" size={size} color={color}/>
                        }

                        return <Ionicons name="albums-outline" size={size} color={color}/>
                    }
                }}
            />
            {/* <Tab.Screen
                name="reels"
                component={Saudacoes}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="hand-left" size={size} color={color}/>
                        }

                        return <Ionicons name="hand-left-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="profile"
                component={Inatividade}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="ban" size={size} color={color}/>
                        }

                        return <Ionicons name="ban-outline" size={size} color={color}/>
                    }
                }}
            />  */}
                        
        </Tab.Navigator>
    );
}

export default Routes;