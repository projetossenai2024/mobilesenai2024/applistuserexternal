import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList
} from 'react-native'
import React, {useState, useEffect} from 'react'
import axios from 'axios';
import { useNavigation } from "@react-navigation/native";


const List = ({ navigation }) => {

    const [users, setUsers] = useState([]); useEffect(() => {
        getUsers()
    }, [])

    async function getUsers() {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            setUsers(response.data);
        } catch (error) {
            console.error(error)
        }
    }

    const userPress = (users) => {
        navigation.navigate("Details", { users });
    };
    return (
        <View style={styles.container}>
            <Text style={styles.textUser}>Users</Text>
            <FlatList
                data={users}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                    <View style={styles.list}>
                        <TouchableOpacity 
                            onPress={() => userPress(item)}
                            >
                            <Text style={styles.textContainer}>
                                {item.name}
                            </Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>

    )
}

export default List

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 35,
    },
    textUser: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    textContainer: {
        backgroundColor: 'lightblue',
        margin: 5,
        borderRadius: 8,
        fontSize: 16,
        fontWeight: '800',
        lineHeight: 24,
        padding: 10
    }

})
