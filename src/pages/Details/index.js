import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Details = ({ route }) => {
    const { users } = route.params;

    return (
        <View style={styles.container}>
            <Text style={styles.title}> Details</Text>
            <Text style={styles.info}>Username : {users?.username}</Text>
            <Text style={styles.info}>Email : {users?.email}</Text>
            <Text style={styles.info}>Phone : {users?.phone}</Text>
            <Text style={styles.info}>Website : {users?.website}</Text>
            <Text style={styles.info}>Street : {users?.address.street}</Text>
            <Text style={styles.info}>Suite : {users?.address.suite}</Text>
            <Text style={styles.info}>City : {users?.address.city}</Text>
            <Text style={styles.info}>Zipcode : {users?.address.zipcode}</Text>
            <Text style={styles.info}>Company Name : {users?.company.name}</Text>
            <Text style={styles.info}>Phrase : {users?.company.catchPhrase}</Text>
        </View>
    )
}

export default Details

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 40,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    info: {
        fontSize: 16,
        lineHeight: 24,
        backgroundColor: "lightblue",
        padding: 10,
        margin: 5,
        borderRadius: 8,
        fontWeight: '700'
    },
})